package dam.androidisrael.u3t2implicitintents;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import java.util.Objects;

public class OtherIntents extends AppCompatActivity implements View.OnClickListener {

    public static final String OTHER_INTENTS = "OtherIntents";
    private EditText etTime, etSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_intents);

        setUI();
    }

    private void setUI() {
        Objects.requireNonNull(getSupportActionBar()).hide();
        etTime = findViewById(R.id.etTime);
        etSearch = findViewById(R.id.etSearch);
        Button btSetAlarm = findViewById(R.id.btSetAlarm);
        Button btOpenCamera = findViewById(R.id.btOpenCamera);
        Button btSearchWeb = findViewById(R.id.btSearchWeb);

        btSetAlarm.setOnClickListener(this);
        btOpenCamera.setOnClickListener(this);
        btSearchWeb.setOnClickListener(this);
    }

    public void createAlarm(int hour, int minutes) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else{
            Log.d(OTHER_INTENTS, " Alarm: Can't handle this intent!");
        }
    }

    public void capturePhoto() {
        Intent intent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else{
            Log.d(OTHER_INTENTS, " openCamera: Can't handle this intent!");
        }
    }

    public void searchWeb(String query) {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, query);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else{
            Log.d(OTHER_INTENTS, " searchWeb: Can't handle this intent!");
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btSetAlarm:
                try{
                    String[] time = etTime.getText().toString().split(":",2);
                    int hour = Integer.parseInt(time[0]);
                    int minutes = Integer.parseInt(time[1]);
                    if((hour<1||hour>24)||(minutes<1||minutes>60)){
                        throw new Exception("Incorrect format");
                    } else{
                        createAlarm(hour,minutes);
                    }
                } catch (Exception e){
                    Log.d(OTHER_INTENTS, " etTime: Not correct format.");
                    etTime.setError("Incorrect format | correct_ej: 13:10");
                }
                break;
            case R.id.btOpenCamera:
                capturePhoto();
                break;
            case R.id.btSearchWeb:
                searchWeb(etSearch.getText().toString());
                break;
        }
    }
}