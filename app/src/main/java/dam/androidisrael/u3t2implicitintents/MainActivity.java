package dam.androidisrael.u3t2implicitintents;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String IMPLICIT_INTENTS = "ImplicitIntents";
    private EditText etUri, etLocation, etText, etZoom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        etUri = findViewById(R.id.etUri);
        etLocation = findViewById(R.id.etLocation);
        etText = findViewById(R.id.etText);
        Button btOpenUri = findViewById(R.id.btOpenUri);
        Button btOpenLocation = findViewById(R.id.btOpenLocation);
        Button btShareText = findViewById(R.id.btShareText);

        btOpenUri.setOnClickListener(this);
        btOpenLocation.setOnClickListener(this);
        btShareText.setOnClickListener(this);
    }

    private void openWebsite(String urlText){
        Uri webpage = Uri.parse(urlText);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        if(intent.resolveActivity(getPackageManager())!= null){
            startActivity(intent);
        } else{
            Log.d(IMPLICIT_INTENTS, " OpenWebsite: Can't handle this intent!");
        }
    }

    private void openLocation(String location){
        Uri addressUri = Uri.parse("geo:0,0?q="+location);
        Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);

        if(intent.resolveActivity(getPackageManager())!= null){
            startActivity(intent);
        } else{
            Log.d(IMPLICIT_INTENTS, " OpenLocation: Can't handle this intent!");
        }
    }

    private void shareText(String text){
        new ShareCompat.IntentBuilder(this).setType("text/plain").setText(text).startChooser();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btOpenUri:
                openWebsite(String.valueOf(etUri.getText()));
                break;
            case R.id.btOpenLocation:
                    openLocation(String.valueOf(etLocation.getText()));
                break;
            case R.id.btShareText:
                shareText(String.valueOf(etText.getText()));
                break;
        }
    }
}