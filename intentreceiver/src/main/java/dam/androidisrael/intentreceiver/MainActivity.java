package dam.androidisrael.intentreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        Intent intent = getIntent();
        TextView text = findViewById(R.id.Text);
        text.setText(intent.getStringExtra(Intent.EXTRA_TEXT));
    }


}